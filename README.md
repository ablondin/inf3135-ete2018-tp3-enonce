# Travail pratique 3

Dans ce troisième (et dernier!) travail pratique, vous devez concevoir un
programme qui charge des données géographiques sous format JSON provenant du
projet [countries](https://github.com/mledoze/countries) et qui effectue
certains traitements de ces données.

Le travail peut être réalisé **seul** ou en équipe d'au plus **3 étudiants**.
Il doit être remis au plus tard le **15 août 2018** à **23h59**.  À partir de
minuit, une pénalité de **2% par heure** de retard sera appliquée.

**Attention**. Je me réserve la possibilité d'attribuer une note complètement
différente (pouvant aller jusqu'à zéro) à un membre d'une équipe dont le
travail n'est pas jugé suffisant. Pour cela, j'étudierai la division des tâches
et les *commits* (quantité et qualité) qui ont été produits par chacun des
membres.

## Objectifs spécifiques

Les principaux objectifs visés sont les suivants :

- **Construire** un logiciel fonctionnel en C à partir de zéro;
- Utiliser correctement un **logiciel de contrôle de version** pour
  **collaborer** (travailler en équipe) et **structurer** le développement (en
  équipe ou seul);
- Se familiariser avec une **bibliothèque C** (Jansson) en consultant sa
  documentation;
- Interagir, à partir d'un programme C, avec un **logiciel externe**
  (Graphviz);
- **Livrer** une application en facilitant sa **distribution** et son
  **utilisation**.

## Description du travail

Vous devez concevoir un petit programme dont l'exécutable est nommé `tp3` et
qui permet d'afficher des informations sur différents pays.

Plus précisément, les informations suivantes doivent être gérées:

- Les langues qui y sont parlées (`--show-languages`);
- La capitale (`--show-capital`);
- Les pays frontaliers (`--show-borders`);
- Le drapeau (`--show-flag`).

Par défaut, tous les pays sont traités, mais il est possible de restreindre
l'affichage:

1) À l'aide de l'argument `--country COUNTRY`, on peut afficher un seul pays,
par exemple:

```sh
$ bin/tp3 --country can --show-languages --show-capital --show-borders
Name: Canada               
Code: CAN                  
Capital: Ottawa            
Languages: English, French 
Borders: USA               
```

2) À l'aide de l'argument `--region REGION`, on peut afficher seulement les
pays d'une région (continent), par exemple:

```sh
$ bin/tp3 --region oceania --show-languages --show-capital --show-borders
Name: American Samoa      
Code: ASM                 
Capital: Pago Pago        
Languages: English, Samoan
Borders:                  
Name: Australia    
Code: AUS          
Capital: Canberra  
Languages: English 
Borders:           
...
Name: Samoa               
Code: WSM                 
Capital: Apia             
Languages: English, Samoan
Borders:                  
```

Notez que selon la base de données `countries`, plusieurs pays d'Océanie ne
partagent pas de frontière avec un autre pays, probablement parce qu'il s'agit
d'îles.

Le message d'aide complet devrait ressembler à ceci :

```sh
Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]
 [--show-languages] [--show-capital] [--show-borders] [--show-flag]
 [--country COUNTRY] [--region REGION]

Displays information about countries.

Optional arguments:
  --help                     Show this help message and exit.
  --output-format FORMAT     Selects the ouput format (either "text", "dot" or "png").
                             The "dot" format is the one recognized by Graphviz.
                             The default format is "text".
  --output-filename FILENAME The name of the output filename. This argument is
                             mandatory for the "png" format. For the "text" and "dot"
                             format, the result is printed on stdout if no output
                             filename is given.
  --show-languages           The official languages of each country are displayed.
  --show-capital             The capital of each country is displayed.
  --show-borders             The borders of each country are displayed.
  --show-flag                The flag of each country is displayed
                             (only for "dot" and "png" format).
  --country COUNTRY          The country code (e.g. "can", "usa") to be displayed.
  --region REGION            The region of the countries to be displayed.
                             The supported regions are "africa", "americas",
                             "asia", "europe" and "oceania".
```

## Récupération des données

Les données géographiques sont disponibles dans le projet
[countries](https://github.com/mledoze/countries) sous plusieurs formats, dont
le format JSON qui nous intéresse plus particulièrement.

On y trouve également des images des drapeaux au format SVG. Cependant, comme
il peut être un peu plus compliqué de manipuler le format SVG avec Graphviz,
j'ai également converti chacune des images au format PNG. Vous pouvez récupérer
ces images en récupérant la branche `png-flags` de ma [copie (*fork*) du
projet](https://github.com/ablondin/countries).

Il existe plusieurs façons d'intégrer un autre projet versionné par Git
(`countries`) comme sous-projet d'un projet sur lequel on travaille (votre
TP3). Par conséquent, vous pouvez choisir l'une des façons suivantes:

1. Vous pouvez simplement cloner ma copie, récupérer les données et les images
   qui vous intéressent et les recopier dans des sous-répertoires que vous
   placerez directement dans votre projet.
2. Vous pouvez utiliser les [sous-modules
   Git](https://git-scm.com/docs/git-submodule). Cette dernière option est une
   occasion pour vous d'en découvrir plus sur Git et en particulier sur la
   gestion de projets multiples, mais elle n'est pas obligatoire.

## Formats de sortie

Vous devez supporter trois formats de sortie:

1) Le format texte (`--output-format text`), qui affiche sous forme textuelle
les informations demandées (l'option `--show-flag` est ignorée dans ce cas).
Par exemple:

```sh
$ bin/tp3 --country can
Name: Canada               
Code: CAN                  
```

2) Le format Graphviz (`--output-format dot`), qui affiche sur la sortie
standard un fichier respectant le format `dot` de Graphviz. Par exemple:

```sh
$ bin/tp3 --country can --show-languages --show-capital --show-borders --show-flag --output-format dot
graph {
    can [
        shape = none,
        label = <<table border="0" cellspacing="0">
            <tr><td align="center" border="1" fixedsize="true" width="200" height="100"><img src="can.png" scale="true"/></td></tr>
            <tr><td align="left" border="1"><b>Name</b>: Canada</td></tr>
            <tr><td align="left" border="1"><b>Code</b>: CAN</td></tr>
            <tr><td align="left" border="1"><b>Capital</b>: Ottawa</td></tr>
            <tr><td align="left" border="1"><b>Language</b>: French, English</td></tr>
            <tr><td align="left" border="1"><b>Borders</b>: USA</td></tr>
        </table>>
    ];
}
```

Ensuite, il est possible de passer ce fichier à Graphviz pour en faire un rendu
à l'aide de la commande

```sh
$ bin/tp3 --country can --show-languages --show-capital --show-borders --show-flag --output-format dot --output-filename canada.dot
$ neato -Goverlap=false -Tpng -o canada.png canada.dot
```

et on obtient l'image suivante:

![](images/canada.png)

3) Le format PNG (`--output-format png`), qui produit une image au format PNG
d'une carte représentée à l'aide de Graphviz. Cette option est obtenue
simplement en utilisant l'option `--output-format png` et en invoquant le
logiciel Graphviz à l'aide d'un appel système.  Par exemple, la commande

```sh
$ bin/tp3 --country can --output-format png --output-filename canada.png
```

devrait produire un fichier PNG nommé `canada.png`.

## Affichage de plusieurs pays

Lorsque plus d'un pays est affiché au format "dot" ou "png", il faut également
insérer les liens entres les pays qui partagent une frontière. Par exemple,
supposons que la France, l'Espagne, l'Italie, l'Allemagne et le Portugal forme
une seule région (ce n'est pas le cas dans les données, mais ça fait un exemple
plus court). Alors on obtiendrait un fichier au format "dot" qui ressemblerait
à

```dot
graph {
    fra [
        shape = none,
        label = <<table border="0" cellspacing="0">
                    <tr><td align="center" border="1" fixedsize="true" width="200" height="100"><img src="fra.png" scale="true"/></td></tr>
                    <tr><td align="left" border="1"><b>Name</b>: France</td></tr>
                    <tr><td align="left" border="1"><b>Code</b>: FRA</td></tr>
                </table>>
    ];
    esp [
        shape = none,
        label = <<table border="0" cellspacing="0">
                    <tr><td align="center" border="1" fixedsize="true" width="200" height="100"><img src="esp.png" scale="true"/></td></tr>
                    <tr><td align="left" border="1"><b>Name</b>: Espagne</td></tr>
                    <tr><td align="left" border="1"><b>Code</b>: ESP</td></tr>
                </table>>
    ];
    prt [
        shape = none,
        label = <<table border="0" cellspacing="0">
                    <tr><td align="center" border="1" fixedsize="true" width="200" height="100"><img src="prt.png" scale="true"/></td></tr>
                    <tr><td align="left" border="1"><b>Name</b>: Portugal</td></tr>
                    <tr><td align="left" border="1"><b>Code</b>: PRT</td></tr>
                </table>>
    ];
    ita [
        shape = none,
        label = <<table border="0" cellspacing="0">
                    <tr><td align="center" border="1" fixedsize="true" width="200" height="100"><img src="ita.png" scale="true"/></td></tr>
                    <tr><td align="left" border="1"><b>Name</b>: Italy</td></tr>
                    <tr><td align="left" border="1"><b>Code</b>: ITA</td></tr>
                </table>>
    ];
    deu [
        shape = none,
        label = <<table border="0" cellspacing="0">
                    <tr><td align="center" border="1" fixedsize="true" width="200" height="100"><img src="deu.png" scale="true"/></td></tr>
                    <tr><td align="left" border="1"><b>Name</b>: Germany</td></tr>
                    <tr><td align="left" border="1"><b>Code</b>: GER</td></tr>
                </table>>
    ];
    fra -- esp;
    fra -- deu;
    fra -- ita;
    esp -- prt;
}
```

Et l'image résultante après appel de Graphviz ressemblerait à:

![](images/region.png)

## Makefile et tests automatiques

Lors de la remise, vous devez inclure un fichier `Makefile` permettant de
compiler votre projet à l'aide de la commande
```sh
make
```

Vous devrez également joindre des suites de tests unitaires qui démontrent que
votre programme est juste et robuste. Plus précisément, on s'attend à ce que la
commande
```sh
make test
```
lance toutes les suites de tests que vous avez conçues. Vous devrez utiliser
autant CUnit que Bats pour vos tests unitaires. La qualité de la couverture
sera également prise en considération.

Finalement, vous devez ajouter un fichier `.gitlab-ci.yml` qui active
l'intégration continue. Ce fichier devra en particulier contenir la cible
`build`, qui permet de vérifier si le projet compile correctement avec toutes
les dépendances, et la cible `test`, qui vérifie si tous les tests que vous
avez conçus terminent avec succès.

## Fichier README

Vous devez inclure un fichier README avec votre projet afin de documenter
différents aspects de celui-ci. Vous devez minimalement inclure les éléments
suivants, mais n'hésitez pas à en ajouter si nécessaire.

```md
# Travail pratique 3

## Description

Description du projet en quelques phrases.
Mentionner le contexte (cours, sigle, université, etc.).

## Auteurs

- Prénom et nom (Code permanent)
- Prénom et nom (Code permanent)
- Prénom et nom (Code permanent)

## Fonctionnement

Expliquez comment fonctionne le programme, quel est son but, quelles sont les
commandes qu'on doit entrer pour le compiler et ensuite l'exécuter, avec au
moins un exemple d'utilisation à l'appui (commande + résultat attendu). Inviter
également le lecteur à s'assurer d'avoir installé toutes les dépendances en
consultant la section [dépendances](#dépendances).

## Plateformes supportées

Indiquez toutes les distributions (Linux, MacOS, etc.) sur lesquelles vous avez
testé l'application, avec la version de chacune des distributions.

## Dépendances

Décrivez toutes les dépendances de votre projet, autant au niveau des logiciels
(par exemple Graphviz) que des bibliothèques (par exemple Jansson).  Fournissez
les liens vers les sites officiels des dépendances s'ils existent.

## Références

Citez vos sources ici, s'il y a lieu.

## Division des tâches

Donnez ici une liste des tâches de chacun des membres de l'équipe. Utilisez
la syntaxe suivante (les crochets vides indiques que la tâche n'est pas
complétée, les crochets avec un `X` indique que la tâche est complétée).
Même si vous travaillez seul, indiquez les tâches que vous avez accomplies
(cela permet de structurer le développement).

- [ ] Programmation du menu principal (Alice)
- [X] Conception des plateformes (Charlie)
- [ ] Gestion des collisions (Charlie)
- [ ] Gestion des feuilles de sprites (Alice)
- [ ] Ajout de sons (Charlie)

  - [ ] lorsque le personnage saute;
  - [ ] lorsque le personnage atterrit;
  - [X] lorsque le personnage prend un beigne;

## État du projet

Indiquer si le projet est complété et sans bogue. Sinon, expliquez ce qui
manque ou ce qui ne fonctionne pas.
```

## Contraintes additionnelles

Afin d'éviter des pénalités, il est important de respecter les contraintes
suivantes :

- Le nom de votre projet doit être exactement `inf3135-ete2018-tp3`.
- L'URL de votre projet doit être exactement
  `https://gitlab.com/<nom d'utilisateur>/inf3135-ete2018-tp3`, où
  `<nom d'utilisateur>` est l'identifiant GitLab d'un membre de l'équipe.
- **Aucune variable globale** (à l'exception des constantes) n'est permise;
- Vos modifications doivent **compiler** sans **erreur** et sans
  **avertissement** lorsqu'on entre `make`.
- Vous devez donner accès en mode *Developer* à l'utilisateur `ablondin`.

Advenant le non-respect d'une ou plusieurs de ces contraintes, une pénalité de
**50%** sera appliquée.

## Remise

Votre travail doit être remis au plus tard le **15 août 2018** à **23h59**.  À
partir de minuit, une pénalité de **2% par heure** de retard sera appliquée.

La remise se fait **obligatoirement** par l'intermédiaire de la plateforme
[GitLab](https://about.gitlab.com/>).  **Aucune remise par courriel ne sera
acceptée** (le travail sera considéré comme non remis).

Les travaux seront corrigés sur une de mes machines personnelles (MacOS,
Ubuntu) et sur le serveur Java.

## Barème

Votre travail sera évalué selon les critères de correction suivants (sur un
total de **200 points**):

- Fonctionnabilité: **100 points**.

  * Gestion des options `--show-languages`, `--show-capital`, `show-borders`:
    **20 points**;
  * Gestion de l'option `--show-flag`: **10 points**;
  * Gestion des options `--country` et `--region`: **20 points**;
  * Format de sortie `text`: **20 points**;
  * Format de sortie `dot`: **20 points**;
  * Format de sortie `png`: **10 points**;

- Qualité de la remise: **60 points**.

  * Compilation simple (`make`): **10 points**.
  * Qualité de la couverture de tests (`make test`) et intégration continue
    (`.gitlab-ci.yml`): **20 points**.
  * Documentation du fonctionnement (`README`): **30 points**.

- Gestion du projet: **40 points**.

  * La grande majorité des modifications devraient être ajoutées par **requêtes
    d'intégration**, afin de bien structurer le développement. Notez que le
    texte qui explique la requête d'intégration n'a pas à être aussi détaillé
    que pour le 2e travail pratique, une courte explication étant suffisante.
  * Si vous êtes en **équipe**, vous pouvez désigner un membre unique de
    l'équipe comme celui qui accepte ou refuse les requêtes d'intégration (il
    joue le rôle du *release manager*). Il est aussi acceptable d'intégrer une
    requête d'intégration dès qu'au moins deux personnes différentes l'ont
    évaluée.
  * Si vous travaillez **seul**, vous devez tout de même structurer vos
    modifications à l'aide de requêtes d'intégration, que vous accepterez
    vous-même. C'est sûr que c'est un peu plus artificiel, mais ça permet au
    moins de bien étudier, rétroactivement, le développement de votre
    programme. En particulier, assurez-vous de laisser toutes les branches que
    vous avez utilisées et fusionnées sur le dépôt afin qu'on puisse les
    étudier si nécessaire.
