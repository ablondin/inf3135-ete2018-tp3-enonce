.PHONY: html

html:
	pandoc -s -f markdown -o README.html README.md -c misc/github-pandoc.css
